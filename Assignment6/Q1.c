#include <stdio.h>

int main()
{
    int i, j, n;

    printf("Enter How Many Lines Required: ");
    scanf("%d", &n);
     printf("\n----------- Pattern %d ---------- \n\n" ,n);

    for(i=1; i<=n; i++)
    {
    
        for(j=i; j>=1; j--)
        {
            printf("%d", j);
        }

        printf("\n");
    }

    return 0;
}
