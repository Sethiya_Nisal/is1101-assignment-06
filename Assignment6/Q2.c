#include<stdio.h>
 int fibonacci(int);
 
int main()
{
   int n, i = 0, x;
   printf("Enter the number upto how many lines of Fibonacci Sequence should be print:");
   scanf("%d",&n);
 
   printf("Fibonacci series-------%d\n",n);
 
   for ( x = 1 ; x <= n ; x++ )
   {
      printf("%d\n", fibonacci(i));
      i++; 
   }
 
   return 0;
}
 
int fibonacci(int n)
{
   if ( n == 0 )
      return 0;
   else if ( n == 1 )
      return 1;
   else
      return ( fibonacci(n-1) + fibonacci(n-2) );
} 
